# ics-ans-role-dhcp-relay

Ansible role to install dhcp-relay on a linux router/firewall.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-dhcp-relay
```

## License

BSD 2-clause
